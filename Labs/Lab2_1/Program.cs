﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_1
{
    class Program
    {
        static void Main(string[] args)
        {

            Date d1 = new Date();

            Console.WriteLine("/////////////// For parameterized constructor");
            Console.WriteLine("Enter date");
            int date = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter month");
            int month = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter year");
            int year = Convert.ToInt32(Console.ReadLine());

            Date d2 = new Date { mDate = date,mMonth = month,mYear = year};

            Console.WriteLine(d2.mDate + "-" + d2.mMonth + "-"  +d2.mYear);

        }
    }
}
