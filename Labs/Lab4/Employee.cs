﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Employee
    {
        private int EmpID;
        private string EmpName;
        private string EmpDesignation;
        private double BasicSal;
        private double HRA;
        private double DA;
        private double PF;
        private double GrossSalary;
        private double NetSalary;
        private int DeptID;

        // static variable
        private static int Count = 0;
        private static int autoID = 10000;

        public Employee()
        {
            Count++;
            autoID++;
        }

        // static method to access count of employees
        public static int getCount()
        {
            return Count;
        }

        public int EID { get { return EmpID; } set { EmpID = autoID; } }
        public string EName { get { return EmpName; } set { EmpName = value; } }
        public string EDesignation { get { return EmpDesignation; } set { EmpDesignation = value; } }
        public int EDeptID { get { return DeptID; } set { DeptID = value; } }
        public double EBasicSal { get { return BasicSal; } set { BasicSal = value; } }
        public double EDA { get { return DA; } set { DA = value; } }

        public void getDetails()
        {
            EID = autoID;
            Console.WriteLine("\nProvide details of Employee");
            Console.WriteLine("Employee ID:" + EmpID);
            Console.Write("Enter name:");
            EName = Console.ReadLine();
            Console.Write("Enter designation:");
            EDesignation = Console.ReadLine();
            Console.Write("Enter Department ID:");
            EDeptID = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter basic salary:");
            EBasicSal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter dearness allowance:");
            DA = Convert.ToDouble(Console.ReadLine());

            calculateSalary();


        }

        private void calculateSalary()
        {
            HRA = (8 * BasicSal) / 100;
            PF = (12 * BasicSal) / 100;
            GrossSalary = BasicSal + HRA + 1250;
            NetSalary = GrossSalary + (10000 - PF);
        }

        public void printDetails()
        {
            Console.WriteLine("\n------------Employee Details----------");
            Console.WriteLine("Employee ID:" + EmpID);
            Console.WriteLine("Employee Name:" + EmpName);
            Console.WriteLine("Employee Designation:" + EmpDesignation);
            Console.WriteLine("Employee Department ID:" + DeptID);
            Console.WriteLine("Employee Basic Salary:" + BasicSal);
            Console.WriteLine("Employee HRA:" + HRA);
            Console.WriteLine("Employee DA:" + DA);
            Console.WriteLine("Employee PF:" + PF);
            Console.WriteLine("Employee Gross Salary:" + GrossSalary);
            Console.WriteLine("Employee Net Salary:" + NetSalary);

            Console.WriteLine();
        }

        

        
    }
}
