﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {

        static void Main(string[] args)
        {

            Employee e1 = new Employee();
            e1.getDetails();
            Employee e2 = new Employee();
            e2.getDetails();
            Employee e3 = new Employee();
            e3.getDetails();

            int ch;
            do
            {
                Console.WriteLine("\nPress 1 to show total number of employees.");
                Console.WriteLine("Press 2 to show details of all employees");
                Console.WriteLine("Press 3 to exit");
                Console.Write("Enter your choice:");
                ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        Console.WriteLine("Total number of employees:" + Convert.ToInt32(Employee.getCount()));
                        break;
                    case 2:
                        e1.printDetails();
                        e2.printDetails();
                        e3.printDetails();
                        break;
                    default:
                        break;
                }

            } while (ch != 3);



        }
    }
}
