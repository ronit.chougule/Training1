﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e1 = new Employee();
            e1.getDetails();
            e1.Print();
            Date d1 = new Date();
            d1.Print();
            Circle c1 = new Circle(5);
            c1.Print();
            Rectangle r1 = new Rectangle(2, 6);
            r1.Print();
        }
    }
}
