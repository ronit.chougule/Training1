﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public interface IPrintable
    {
        void Print();
    }

    public class Employee : IPrintable
    {
        private int EmpID;
        private string EmpName;
        private string EmpDesignation;
        private double BasicSal;
        private double HRA;
        private double DA;
        private double PF;
        private double GrossSalary;
        private double NetSalary;
        private int DeptID;


        // static variable
        private static int Count = 0;

        private static int autoID = 10000;

        // Default Constructor
        public Employee()
        {
            Count++;
            autoID++;
        }

        // static method to access count of employees
        public static int getCount()
        {
            return Count;
        }

        public int EID { get { return EmpID; } set { EmpID = value; } }
        public string EName { get { return EmpName; } set { EmpName = value; } }
        public string EDesignation { get { return EmpDesignation; } set { EmpDesignation = value; } }
        public int EDeptID { get { return DeptID; } set { DeptID = value; } }
        public double EBasicSal { get { return BasicSal; } set { BasicSal = value; } }
        public double EDA { get { return DA; } set { DA = value; } }
        public double EHRA { get { return HRA; } set { HRA = value; } }
        public double EPF { get { return PF; } set { PF = value; } }
        public double EGrossSalary { get { return GrossSalary; } set { GrossSalary = value; } }
        public double ENetSalary { get { return NetSalary; } set { NetSalary = value; } }

        public void getDetails()
        {
            EID = autoID;
            Console.WriteLine("\nProvide details of Employee");
            Console.WriteLine("Employee ID:" + EmpID);
            Console.Write("Enter name:");
            EName = Console.ReadLine();
            Console.Write("Enter designation:");
            EDesignation = Console.ReadLine();
            Console.Write("Enter Department ID:");
            EDeptID = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter basic salary:");
            EBasicSal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter dearness allowance:");
            EDA = Convert.ToDouble(Console.ReadLine());

            calculateSalary();
        }

        private void calculateSalary()
        {
            EHRA = (8 * BasicSal) / 100;
            EPF = (12 * BasicSal) / 100;
            EGrossSalary = BasicSal + HRA + 1250;
            ENetSalary = GrossSalary + (10000 - PF);
        }
        public void Print()
        {
            Console.WriteLine();
            Console.WriteLine("Employee ID:" + EID);
            Console.WriteLine("Employee Name:" + EName);
            Console.WriteLine("Employee Designation:" + EDesignation);
        }
    }

    public class Date : IPrintable
    {
        private string date;
        public Date()
        {
            Console.Write("Enter date of joining:");
            date = Console.ReadLine();
        }
        public void Print()
        {
            Console.WriteLine();
            Console.WriteLine("Date of Joining: " + date);
        }
    }

    public abstract class Shape : IPrintable
    {
        public abstract void Print();
    }

    public class Circle : Shape
    {
        private int radius;
        private double area;
        public Circle(int _radius)
        {
            radius = _radius;
        }
        public override void Print()
        {
            area = Math.PI * radius * radius;
            Console.WriteLine();
            Console.WriteLine("Area of circle:" + area);
        }
    }

    public class Rectangle : Shape
    {
        private int length;
        private int breadth;
        private double area;
        public Rectangle(int _length, int _breadth)
        {
            length = _length;
            breadth = _breadth;
        }
        public override void Print()
        {
            area = length * breadth;
            Console.WriteLine();
            Console.WriteLine("Area of rectangle:" + area);
        }
    }
}
