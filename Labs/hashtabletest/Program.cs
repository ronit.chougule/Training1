﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;
namespace hashtabletest
{
    class Program
    {
    
        static void Main(string[] args)
        {
            Hashtable hashtable = collectwords();
            displayhashtable(hashtable);
            Console.ReadLine();
        }

        private static void displayhashtable(Hashtable hashtable)
        {
            Console.WriteLine("\n Hashtable contains: \n{0,-12}{1,-12}", "key:","Value:");
            foreach (var key in hashtable.Keys)
            {
                Console.WriteLine("{0,-12}{1,-12}",key,hashtable[key]);
            }

            Console.WriteLine("\n size : {0}",hashtable.Count);
        }

        private static Hashtable collectwords()
        {
            Hashtable table = new Hashtable();
            Console.WriteLine("Enter a string: "); 
            string input = Console.ReadLine();
            string[] words = Regex.Split(input, @"\s+");

            foreach (var word in words)
            {
                string wordkey = word.ToLower();

                if (table.ContainsKey(wordkey))
                {
                    table[wordkey] = ((int)table[wordkey] + 1);
                }
                else
                {
                    table.Add(wordkey, 1);
                }
            }
            return table;
        }
    }
}
