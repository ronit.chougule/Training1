﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace arraylisttest
{
    class Program
    {
        private static readonly string[] colors = { "Magenta ", "red ", "white ", "blue ", "cyan " };
        private static readonly string[] removecolors = { "red", "white", "blue" };
             
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(1);
            Console.WriteLine("Base capacity " + list.Capacity);
            // add elements from static array into this list 
            foreach (var color in colors)
            {
                list.Add(color);
            }

            ArrayList removelist = new ArrayList(removecolors);
            displayinfo(list);

            //remove colors
            removecolor(list,removelist);
            displayinfo(list);
            Console.ReadLine();
        }

        private static void removecolor(ArrayList list, ArrayList removelist)
        {
            for (int count = 0; count < removelist.Count; count++)
            {
                list.Remove(removelist[count]);
                Console.WriteLine("\n Arraylist After removing colors: ");
                
            }
        }

        private static void displayinfo(ArrayList list)
        {
            //iterate through list
            foreach (var item in list)
            {
                Console.Write("{0}", item);
            }
            Console.WriteLine("\n size: {0}; capacity:{1}",list.Count,list.Capacity);
            int index = list.IndexOf("blue");
            if (index!= -1)
            {
                Console.WriteLine("The array list contains blue at index {0}",index);
            }
            else
            {
                Console.WriteLine("Does not found");
            }
        }
    }
}
