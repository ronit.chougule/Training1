﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class Employee
    {

        private int EmpID;
        private string EmpName;
        private string EmpDesignation;
        private double BasicSal;
        private double HRA;
        private double DA;
        private double PF;
        private double GrossSalary;
        private double NetSalary;
        private int DeptID;


        public Employee(double _basicSalary,double _DA)
        {
            HRA = (8 * _basicSalary) / 100;
            PF = (12 * _basicSalary) / 100;
            GrossSalary = _basicSalary + HRA + 1250;
            NetSalary = GrossSalary + (10000 - PF);
            
        }

        public int EmployeeID { get { return EmpID; } set { EmpID = value; } }
        public string EmployeeName { get { return EmpName; } set { EmpName = value; } }
        public string EmployeeDesignation { get { return EmpDesignation; } set { EmpDesignation = value; } }
        public double EmployeeBasicSalary { get { return BasicSal; } set { BasicSal = value; } }
        public double EmployeeHRA { get { return HRA; } }
        public double EmployeeDA { get { return DA; } set { DA = value; } }
        public double EmployeePF { get { return PF; } }
        public double EmployeeGrossSalary { get { return GrossSalary; } }
        public double EmployeeNetSalary { get { return NetSalary; }}
        public int EmpDeptID { get { return DeptID; } set { DeptID = value; } }



    }
}
