﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_2
{
    class Department
    {
        private int DeptID;
        private string DeptName;
        private string DeptLocation;

        public int DepartmentID {
            get
            {
                return DeptID;
            }
            set
            {
                DeptID = value;
            }
        }

        public string DepartmentName
        {
            get
            {
                return DeptName;
            }
            set
            {
                DeptName = value;
            }
        }

        public string DepartmentLocation
        {
            get
            {
                return DeptLocation;
            }
            set
            {
                DeptLocation = value;
            }
        }

    }
}
