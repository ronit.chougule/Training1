﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace stacktest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aboolean = true;
            char achar = '$';
            int anint = 34567;
            string astring = "Hello";
            //adding elements in stack
            //use pushh method
            stack.Push(aboolean);
            printstack(stack);
            stack.Push(achar);
            printstack(stack);
            stack.Push(anint); 
            printstack(stack);
            stack.Push(astring);
            printstack(stack);
            
            Console.WriteLine( "First item in stack is {0}\n",stack.Peek());
            printstack(stack);
            object o = stack.Pop();
            printstack(stack);
            Console.ReadLine();
        }

        private static void printstack(Stack stack)
        {
            if (stack.Count==0)
            {
                Console.WriteLine("Stack is empty\n");
            }
            else
            {
                Console.WriteLine("stack is :");
                foreach (var item in stack)
                {
                    Console.WriteLine("{0} ",item);
                }
                Console.WriteLine();
            }
        }
    }
}
